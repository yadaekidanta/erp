<?php

namespace App\Http\Controllers\Invtation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ReplyJson;
use App\Traits\InvitationView;

class TransactionController extends Controller
{
    use InvitationView, ReplyJson;
    public function index()
    {
        $tanggal = date('2022-03-20');
        return $this->render_view('transaction.main',compact('tanggal'));
        return view('page.invitation.desktop.transaction.main',compact('tanggal'));
    }
    public function modal_prokes()
    {
        return $this->render_view('transaction.modal_prokes');
    }
    public function show()
    {
        $tanggal = date('2022-03-20');
        return $this->render_view('transaction.show',compact('tanggal'));
    }
}
