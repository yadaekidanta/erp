<?php
namespace App\Traits;
use Jenssegers\Agent\Agent;
trait InvitationView{
    public function render_view($view,$compact=null,$alternativePath=null){
        $agent = new Agent();
        if($agent->isMobile()){
            if(view()->exists('page.invitation.mobile.'.$view)){
                #return view mobile
                if($compact!=null){
                    return view('page.invitation.mobile.'.$view,$compact);
                }else{
                    return view('page.invitation.mobile.'.$view);
                }
            }else{
                return redirect($alternativePath);
            }
        }else{
            if(view()->exists('page.invitation.desktop.'.$view)){
                #return view frontend
                if($compact!=null){
                    return view('page.invitation.desktop.'.$view,$compact);
                }else{
                    return view('page.invitation.desktop.'.$view);
                }
            }else{
                return redirect($alternativePath);
            }
        }
    }
}