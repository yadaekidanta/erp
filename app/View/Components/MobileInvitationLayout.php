<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MobileInvitationLayout extends Component
{
    public $title;
    public function __construct($title)
    {
        $this->title = $title;
    }
    public function render()
    {
        return view('theme.mobile_invitation.main');
    }
}
