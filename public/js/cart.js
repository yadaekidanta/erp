load_cart(localStorage.getItem("route_cart"));
function tombol_cart(){
    load_cart(localStorage.getItem("route_cart"));
}
function load_cart(url){
    // let data = "view="+ view + "&load_keranjang=";
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',
        success: function (response){
            $('.top-cart-items').html(response.collection);
            $('.top-cart-number').html(response.total_item) ?? 0;
            // $('#counter_cart').html(format_ribuan(response.total_item) ?? 0);
        },
    });
}
$(document).on('click', '#top-cart', function(){
    load_cart(localStorage.getItem("route_cart"));
});