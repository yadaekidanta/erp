<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Invitation\MainController;
use App\Http\Controllers\Invtation\TransactionController;

Route::group(['domain' => ''], function() {
    Route::prefix('invitation')->name('invitation.')->group(function(){
        Route::get('',[MainController::class, 'index'])->name('index');
        Route::get('demo/{demo:slug}',[DemoController::class, 'show'])->name('demo.show');
        Route::get('eddy-riri',[TransactionController::class, 'index'])->name('transaction.index');
        Route::post('eddy-riri/prokes',[TransactionController::class, 'modal_prokes'])->name('transaction.modal_prokes');
        Route::get('eddy-riri/show',[TransactionController::class, 'show'])->name('transaction.show');
    });
});