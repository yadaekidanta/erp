<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\MainController;

Route::group(['domain' => ''], function() {
    Route::name('web.')->group(function(){
        Route::get('',[MainController::class, 'index'])->name('index');
    });
});