<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('canvas/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('canvas/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('canvas/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('canvas/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('canvas/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('canvas/css/magnific-popup.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{asset('canvas/css/custom.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>{{$title}}</title>

</head>