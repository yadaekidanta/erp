<!DOCTYPE html>
<html dir="ltr" lang="en-US">
@include('theme.web_invitation.head')
<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
		@include('theme.web_invitation.slider')
		<!-- Header
		============================================= -->
        @include('theme.web_invitation.header')
		<!-- #header end -->

		<!-- #content end -->
        {{$slot}}
		<!-- Footer
		============================================= -->
        @include('theme.web_invitation.footer')
		<!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	@include('theme.web_invitation.js')
    @yield('custom_js')
</body>
</html>