<!DOCTYPE html>
<html lang="en">
    @include('theme.invitation.head')
    <body class="gla_middle_titles">
        <div class="gla_page" id="gla_page">
            <!-- Page -->
            {{-- <div class="gla_page gla_image_bck" data-image="http://placehold.it/1400x800" data-color="#f2f2f2" id="gla_page" > --}}
            <!-- Content -->
            {{$slot}}
            <!-- Content End -->
            <!-- Page End -->
            <!-- JQuery -->
        </div>
        @include('theme.invitation.modal')
        @include('theme.invitation.js')
        {{-- <div id="music"><embed src="{{asset('music/source.mp3')}}" autostart=true loop=true></div> --}}
        @yield('custom_js')
    </body>
</html>