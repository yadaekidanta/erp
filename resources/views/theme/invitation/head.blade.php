<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$title}}</title>
    <!-- Favicon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    
    <!-- Library CSS -->
    <link href="{{asset('glanz/css/glanz_library.css')}}" rel="stylesheet">
    
    <!-- Icons CSS -->
    <link href="{{asset('glanz/fonts/themify-icons.css')}}" rel="stylesheet">
    
    <!-- Theme CSS -->
    <link href="{{asset('glanz/css/glanz_style.css')}}" rel="stylesheet">
    
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Dosis:300,400,600,700%7COpen+Sans:300,400,700%7CPlayfair+Display:400,400i,700,700i" rel="stylesheet">
    
    <!-- Other Fonts -->
    <link href="{{asset('glanz/fonts/marsha/stylesheet.css')}}" rel="stylesheet">
    <link href="{{asset('css/invitation.css')}}" rel="stylesheet">
</head>