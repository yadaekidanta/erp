<section id="slider" class="slider-element slider-parallax vh-100">
    <div class="slider-inner" style="background: url('{{asset('canvas/images/parallax/home/9.jpg')}}') center center no-repeat; background-size: cover;">
        <div class="vertical-middle slider-element-fade">
            <div class="container">
                <div class="emphasis-title">
                    <h1 data-animate="fadeInUp"><strong>Future Proof Your Business</strong></h1>
                    <h3>Enterprise Digital Transformation Consultant & Integrator</h3>
                </div>
            </div>
        </div>
    </div>
</section>