<!DOCTYPE html>
<html dir="ltr" lang="en-US">
@include('theme.web.head')
<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
		@include('theme.web.slider')
		<!-- Header
		============================================= -->
        @include('theme.web.header')
		<!-- #header end -->

		<!-- #content end -->
        {{$slot}}
		<!-- Footer
		============================================= -->
        @include('theme.web.footer')
		<!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	@include('theme.web.js')
    @yield('custom_js')
</body>
</html>