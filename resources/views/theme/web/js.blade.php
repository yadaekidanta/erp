<script src="{{asset('canvas/js/jquery.js')}}"></script>
<script src="{{asset('canvas/js/plugins.min.js')}}"></script>

<!-- Footer Scripts
============================================= -->
<script src="{{asset('canvas/js/functions.js')}}"></script>
<script>
    $(window).scroll(function() {
	    var vpHeight = $(window).height();
        var isBlack = false;
        $("#wrapper").each(function(i, section) {
            if(isBlack) {
    	        return;
            }
            var offset = $(section).offset().top - $(window).scrollTop();
            if(((offset + vpHeight) >= 0) && ((offset + vpHeight) <= vpHeight)) {
                isBlack = true;
                return;
            }
        });
        if ($(".menu-text").hasClass("text-white")) {
            if(!isBlack){
                $(".menu-text").removeClass("text-white");
                $(".icon-line-search").removeClass("text-white");
                $(".icon-line-bag").removeClass("text-white");
            }
        }else{
            if(isBlack){
                $(".menu-text").addClass("text-white");
                $(".icon-line-search").addClass("text-white");
                $(".icon-line-bag").addClass("text-white");
            }
        }
    });
</script>