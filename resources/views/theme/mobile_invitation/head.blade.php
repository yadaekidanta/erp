<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <title>{{$title}}</title>
    <link rel="stylesheet" type="text/css" href="{{asset('azures/styles/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('azures/styles/style.css')}}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Comforter&family=Grand+Hotel&family=Nanum+Myeongjo:wght@400;700;800&family=Pinyon+Script&family=Rochester&family=Satisfy&family=Sofia&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('azures/fonts/css/fontawesome-all.min.css')}}">    
    <link rel="manifest" href="{{asset('azures/_manifest.json')}}" data-pwa-version="set_in_manifest_and_pwa_js">
    <link rel="apple-touch-icon" sizes="180x180" href="app/icons/icon-192x192.png">
    <link rel="stylesheet" class="page-highlight" type="text/css" href="{{asset('azures/styles/highlights/highlight_dark.css')}}">
</head>