<!DOCTYPE HTML>
<html lang="en">
    @include('theme.mobile_invitation.head')
<body class="theme-light" data-highlight="highlight-pink2">
<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>
<div id="page">
    {{$slot}}
</div>
@include('theme.mobile_invitation.js')
@yield('custom_js')
</body>
</html>