<x-web-invitation-layout title="Welcome">
    <!-- Content
    ============================================= -->
    <section id="content">
        <div class="content-wrap py-0">

            <div class="section parallax min-vh-100 dark m-0 border-0 d-flex" style="background-image: url('images/parallax/1.jpg');" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
                <div class="vertical-middle">
                    <div class="container">

                        <div class="row col-mb-50">
                            <div class="col-sm-6 col-lg-3 text-center" data-animate="bounceIn">
                                <i class="i-plain i-xlarge mx-auto mb-0 icon-code"></i>
                                <div class="counter counter-large counter-lined"><span data-from="100" data-to="846" data-refresh-interval="50" data-speed="2000"></span>K+</div>
                                <h5>Lines of Codes</h5>
                            </div>

                            <div class="col-sm-6 col-lg-3 text-center" data-animate="bounceIn" data-delay="200">
                                <i class="i-plain i-xlarge mx-auto mb-0 icon-magic"></i>
                                <div class="counter counter-large counter-lined"><span data-from="3000" data-to="15360" data-refresh-interval="100" data-speed="2500"></span>+</div>
                                <h5>KBs of HTML Files</h5>
                            </div>

                            <div class="col-sm-6 col-lg-3 text-center" data-animate="bounceIn" data-delay="400">
                                <i class="i-plain i-xlarge mx-auto mb-0 icon-file-text"></i>
                                <div class="counter counter-large counter-lined"><span data-from="10" data-to="386" data-refresh-interval="25" data-speed="3500"></span>*</div>
                                <h5>No. of Templates</h5>
                            </div>

                            <div class="col-sm-6 col-lg-3 text-center" data-animate="bounceIn" data-delay="600">
                                <i class="i-plain i-xlarge mx-auto mb-0 icon-time"></i>
                                <div class="counter counter-large counter-lined"><span data-from="60" data-to="1200" data-refresh-interval="30" data-speed="2700"></span>+</div>
                                <h5>Hours of Coding</h5>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="section parallax min-vh-100 m-0 border-0 d-flex" style="background-image: url('images/parallax/home/11.jpg');" data-bottom-top="background-position:-200px -250px;" data-top-bottom="background-position:0px -400px;">
                <div class="vertical-middle">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-5">
                                <div class="emphasis-title">
                                    <h2>Seriously Parallaxical</h2>
                                    <p class="lead topmargin-sm">Create beautiful unlimited full-screen parallax defined backgrounds to spell bound your visitors.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row p-0 align-items-stretch">

                <div class="col-lg-4 dark col-padding overflow-hidden" style="background-color: #1abc9c;">
                    <div>
                        <h3 class="text-uppercase" style="font-weight: 600;">Why choose Us</h3>
                        <p style="line-height: 1.8;">Transform, agency working families thinkers who make change happen communities. Developing nations legal aid public sector our ambitions future aid The Elders economic security Rosa.</p>
                        <a href="#" class="button button-border button-light button-rounded text-uppercase m-0">Read More</a>
                        <i class="icon-bulb bgicon"></i>
                    </div>
                </div>

                <div class="col-lg-4 dark col-padding overflow-hidden" style="background-color: #34495e;">
                    <div>
                        <h3 class="text-uppercase" style="font-weight: 600;">Our Mission</h3>
                        <p style="line-height: 1.8;">Frontline respond, visionary collaborative cities advancement overcome injustice, UNHCR public-private partnerships cause. Giving, country educate rights-based approach; leverage disrupt solution.</p>
                        <a href="#" class="button button-border button-light button-rounded text-uppercase m-0">Read More</a>
                        <i class="icon-cog bgicon"></i>
                    </div>
                </div>

                <div class="col-lg-4 dark col-padding overflow-hidden" style="background-color: #e74c3c;">
                    <div>
                        <h3 class="text-uppercase" style="font-weight: 600;">What you get</h3>
                        <p style="line-height: 1.8;">Sustainability involvement fundraising campaign connect carbon rights, collaborative cities convener truth. Synthesize change lives treatment fluctuation participatory monitoring underprivileged equal.</p>
                        <a href="#" class="button button-border button-light button-rounded text-uppercase m-0">Read More</a>
                        <i class="icon-thumbs-up bgicon"></i>
                    </div>
                </div>

            </div>

            <div class="clear"></div>

            <div class="section m-0 border-0" style="background-image: url('images/parallax/3.jpg');">
                <div class="heading-block center border-bottom-0 mb-0">
                    <h2>"Everything is designed, but some things are designed well."</h2>
                </div>
            </div>

            <div class="section parallax min-vh-100 dark m-0 border-0 d-flex" style="background-image: url('images/parallax/home/10.jpg');"  data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
                <div class="vertical-middle">
                    <div class="container">

                        <div class="row align-items-center gutter-40 col-mb-50">
                            <div class="col-md-6">
                                <iframe src="https://player.vimeo.com/video/243244233?title=0&byline=0&portrait=0" width="640" height="360" allow="autoplay; fullscreen" allowfullscreen></iframe>
                            </div>

                            <div class="col-md-6">
                                <div class="emphasis-title">
                                    <h2>Embed Videos</h2>
                                    <p class="lead topmargin-sm">Show off your Vimeo &amp; Videos in style and with a perfect aspect ratio.</p>
                                </div>

                                <a href="#" class="button button-border button-rounded button-light button-large">Go to Vimeo</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="section parallax dark m-0 border-0" style="padding: 150px 0; background-image: url('images/parallax/home/5.jpg');" data-bottom-top="background-position:0px -300px;" data-top-bottom="background-position:0px 0px;">
                <div class="container text-center">

                    <div class="emphasis-title">
                        <h2>Perfect tool for Customization</h2>
                        <p class="lead topmargin-sm">Create as much unique content as you want with this Template which has powerful &amp; optimized code.</p>
                    </div>

                    <a href="#" class="button button-border button-rounded button-light button-large">Start Browsing</a>
                    <a href="#" class="button button-rounded button-white button-light button-large">Buy Now</a>

                </div>
            </div>
        </div>
    </section>
</x-web-invitation-layout>