<x-invitation-layout title="Undangan Pernikahan : Eddy & Riri">
    <div class="gla_over" data-color="#D59696" data-opacity="0"></div>
    <a href="#gla_page" class="gla_top ti ti-angle-up gla_go"></a>
    <div class="gla_music_icon">
        <i class="ti ti-music" id="music_button"></i>
    </div>
    <div class="gla_music_icon_cont">
        <audio id="audio" loop>
            <source src="{{asset('music/source.mp3')}}" type="audio/mp3" />
        </audio>
    </div>
    <section id="gla_content" class="gla_content">
        <div id="content_list">
            <div class="gla_invitation_container" style="margin-top:-3%;">
                <div class="gla_invitation_i gla_invitation_ii gla_image_bck" data-image="{{asset('img/invitation/pink.png')}}">
                    {{-- <p><img src="{{asset('img/invitation/save_the_date_bl.gif')}}" alt="" height="200"></p> --}}
                    <h4 style="font-size:50px;margin-top:10%">Engagement</h4>
                    <h4 style="font-size:50px;margin-top:10%">Ceremony</h4>
                    <h3>
                        Rianasari
                        <br>
                        &
                        <br>
                        Eddy Mardiwan
                    </h3>
                    <h4 style="font-size:50px;">{{Carbon\Carbon::parse($tanggal)->isoFormat('dddd')}}</h4>
                    <h4 style="font-size:39px;margin-top:10%;">{{Carbon\Carbon::parse($tanggal)->isoFormat('D MMMM Y')}}</h4>
                    {{-- <h4 style="font-size:20px;">Restaurant Bunga Rampai</h4> --}}
                    <br>
                    <button type="button" class="btn medium btn_border" onclick="handle_open_modal('{{route('invitation.transaction.modal_prokes')}}','#Modal','#contentModal');">Buka Undangan</button>
                    {{-- <h4 style="font-size:16px;">
                        Jl. Teuku Cik Ditiro No.35, RT.10/RW.5, Menteng, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10310
                    </h4> --}}
                </div>
            </div>
        </div>
        <div id="content_invitation"></div>
    </section>
    @section('custom_js')
        <script src="{{asset('glanz/js/glanz_library.js')}}"></script> 
        <script src="{{asset('glanz/js/glanz_script.js')}}"></script>
        <script type="text/javascript">
            main_content('content_list');
            $(".gla_music_icon").hide();
            // function confirm_open_invitation(){
            //     $("#invitation").hide();
            //     $(".gla_music_icon").show();
            //     $(".gla_invitation_container").hide();
            //     // audio.play();
            //     $("#slider").show();
            //     $("#list_result").show();
            //     $("#ProkesModal").hide();
            //     $('.modal-backdrop').remove();
            // }
            // $(".gla_music_icon").click(function(){
            //     $('#audio').on('pause', function() {
            //         if (audio.paused) {
            //             audio.play();
            //             $('button.toggle-stream').attr('data-icon','s');
            //         }   
            //         else {
            //             audio.pause();
            //             $('button.toggle-stream').attr('data-icon','p');
            //         }
            //     });
            // });
        </script>
    @endsection
</x-invitation-layout>