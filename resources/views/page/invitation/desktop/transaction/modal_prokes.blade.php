<div class="top-border">
    <img width="40px" src="{{asset('img/prokes/head.png')}}">
</div>
<div class="bottom-border"></div>
<div class="bottom-sign">
    <h3>-E &amp; R-</h3>
</div>
<div class="body p-4" style="border-radius: 7px;overflow: hidden">
    <div style="text-align: center;padding-top: 10px;">
        <h5>Informasi Protokol Kesehatan</h5>
        <p>Acara ini akan dilaksanakan dengan menerapkan protokol kesehatan.</p>
        <ul style="margin-top:20px;">
            <li>
                <div class="item">
                    <img src="{{asset('img/prokes/1.png')}}" alt="">
                    <h4>Memakai Masker</h4>
                </div>
            </li>
            <li>
                <div class="item">
                    <img src="{{asset('img/prokes/2.png')}}" alt="">
                    <h4>Menjaga Jarak</h4>
                </div>
            </li>
            <li>
                <div class="item">
                    <img src="{{asset('img/prokes/3.png')}}" alt="">
                    <h4>Mencuci Tangan</h4>
                </div>
            </li>
        </ul>
    </div>
    <div class="prokes-footer">
        <button type="button" data-dismiss="modal" class="btn medium btn_border" style="margin-top:10%;" onclick="load_invitation('{{route('invitation.transaction.show')}}');">Baik, Saya Mengerti</button>
    </div>
</div>