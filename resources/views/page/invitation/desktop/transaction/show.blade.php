<div class="gla_slider gla_image_bck gla_fixed" data-color="#fbf5ee">
    <div class="gla_slider_flower">
         <div class="gla_slider_flower_c1 gla_slider_flower_iii gla_slider_flower_item" data-start="top:0px; left:0px" data-200-start="top:-300px; left:-300px"></div>
        <div class="gla_slider_flower_c2 gla_slider_flower_iii gla_slider_flower_item" data-start="top:0px; right:0px" data-200-start="top:-300px; right:-300px"></div>
        <div class="gla_slider_flower_c3 gla_slider_flower_iii gla_slider_flower_item" data-start="bottom:0px; right:0px" data-200-start="bottom:-300px; right:-300px"></div>
        <div class="gla_slider_flower_c4 gla_slider_flower_iii gla_slider_flower_item" data-start="bottom:0px; left:0px" data-200-start="bottom:-300px; left:-300px"></div>
        <div class="gla_slider_flower_c5 gla_slider_flower_iii gla_slider_flower_item" data-start="top:0px; left:50%" data-200-start="top:-300px; left:50%"></div>
        <div class="gla_slider_flower_c6 gla_slider_flower_iii gla_slider_flower_item" data-start="bottom:0px; left:50%" data-200-start="bottom:-300px; left:50%"></div>
    </div>
    <div class="gla_over" data-color="#1e1d2d" data-opacity="0"></div>
    <div class="container">
        <div class="gla_slide_txt gla_slide_center_middle text-center">
            <img src="{{asset('img/invitation/save_the_date_bl.gif')}}" data-bottom-top="@src:{{asset('img/invitation/save_the_date_bl.gif')}}" height="150" alt="" style="margin-bottom:-5%;">
             <div class="gla_flower gla_flower2">
                <div class="gla_flower2_name_l">Riri</div>
                <div class="gla_flower2_name_r">Eddy</div>
                <img src="{{asset('img/mempelai/main.jpg')}}" alt="">
            </div>
            <h2 style="margin-top:-7%;margin-bottom:-1%;">{{Carbon\Carbon::parse($tanggal)->format('l, j F Y')}}</h2>
            <div class="gla_countdown" data-year="2022" data-month="03" data-day="20"></div>
        </div>
    </div>
</div>
<section class="gla_section">
    <div class="container text-center">
        <div class="row text-center">
            <div class="col-md-4 gla_round_block">
                <div class="gla_round_im gla_image_bck" data-image="{{asset('img/mempelai/r.jpg')}}"></div>
                <h2>Riri</h2>
                <h3 style="margin-top:-25%;">Rianasari</h3>
                <h4 style="margin-top:-10%;">
                    First child of<br>
                <b>Ayah Daniel syarief</b><br>
                <b>Ibu Herlina</b>
                </h4>
            </div>
            <div class="col-md-4 gla_round_block">
                <p><img src="{{asset('img/invitation/flowers4.gif')}}" data-bottom-top="@src:{{asset('img/invitation/flowers4.gif')}}; opacity:1" class="gla_animated_flower" height="150" alt=""></p>
                {{-- <p><img src="images/animations/mrandmrs.gif" data-bottom-top="@src:images/animations/mrandmrs.gif" height="150" alt=""></p> --}}
                {{-- <h3>Are getting married<br>on August 10, 2017</h3>
                St. Thomas's Church,<br>Bristol, U.K. --}}
            </div>
            <div class="col-md-4 gla_round_block">
                <div class="gla_round_im gla_image_bck" data-image="{{asset('img/mempelai/e.jpg')}}"></div>
                <h2>Eddy</h2>
                <h3 style="margin-top:-25%;">Eddy Mardiwan</h3>
                <h4 style="margin-top:-10%;">
                    3rd child of <br>
                    <b>Almarhum Soemarto Moedjaid</b><br>
                    <b>Ibu Sani Soemarto Moedjaid</b>
                </h4>
            </div>
        </div>
    </div>
</section>
<section class="gla_section gla_lg_padding gla_image_bck gla_fixed gla_wht_txt" data-image="{{asset('img/bg/story.jpeg')}}">
    <div class="gla_over" data-color="#000" data-opacity="0.3"></div>
    <div class="container text-center">
        <p><img src="{{asset('img/invitation/flower5.gif')}}" data-bottom-top="@src:{{asset('img/invitation/flower5.gif')}}; opacity:1" class="gla_animated_flower" height="150" alt=""></p>
        <h2>Our Story</h2>
        {{-- <h3 class="gla_subtitle">The Fourth of July</h3> --}}
        <p>
            The first time I saw Riri was in Lampung in 1991. The story is, we and our friends were on vacation on Sibesi Island, at that time we were just friends.
            However, in 2021 there will be love between us when we meet again, with the support of our friends, so that we can unite so that it lasts until this moment.
            — Eddy
        </p>
    </div>
</section>
<section class="gla_section">
    <div class="container text-center">
        <p><img src="{{asset('img/invitation/flower6.gif')}}" data-bottom-top="@src:{{asset('img/invitation/flower6.gif')}}; opacity:1" class="gla_animated_flower" height="110" alt=""></p>
        <h2>Wedding Details</h2>
        <h3 class="gla_subtitle">When & Where</h3>
        {{-- <p>Our ceremony and reception will be held at the Liberty House. Located on the Hudson River, it has a beautiful, unobstructed view of the World Trade One building and a convenient ferry that runs between it and Manhattan.</p> --}}
        <div class="row text-center">
            <div class="col-md-4 gla_round_block">
                <div class="gla_round_im gla_image_bck" data-image="{{asset('img/galeri/cincin.jpg')}}"></div>
                <h3>Engagement Ceremony</h3>
                <p>{{Carbon\Carbon::parse($tanggal)->format('l, j F Y')}}
                    19.00 WIB<br>
                Jl. Teuku Cik Ditiro No.35, RT.10/RW.5<br>
                Menteng, Kec. Menteng, Kota Jakarta Pusat<br>
                Daerah Khusus Ibukota Jakarta 10310</p>
                <a href="https://goo.gl/maps/H3NNSAH8bpF2TsdU9" target="_blank" class="btn">View Map</a>
            </div>
        </div>
    </div>
</section>
<section class="gla_section gla_image_bck">
    <div class="container text-center">
        <p><img src="{{asset('img/invitation/flower7.gif')}}" data-bottom-top="@src:{{asset('img/invitation/flower7.gif')}}; opacity:1" class="gla_animated_flower" height="110" alt=""></p>
        <h2>The Day They Got Engaged</h2>
        {{-- <p>Andy and Jeska met in university in the Graphic Design program. They both remember each other from orientation, but it wasn’t love at first sight, that’s for sure. Andy remembers Jeska as a ‘snooty art bitch (having been in the visual arts program at the time), and she remembers Andy being an ‘arrogant computer nerd’, boasting his knowledge of Macs over the other students.</p> --}}
        {{-- <div class="button-group filter-button-group">
            <a data-filter="*">Show All</a>
            <a data-filter=".engagement">Engagement</a>
            <a data-filter=".ceremony">Ceremony</a>
        </div> --}}
        <div class="gla_portfolio_no_padding grid">
            {{-- <div class="col-sm-4 gla_anim_box grid-item engagement"> --}}
            <div class="col-sm-4 gla_anim_box grid-item">
                <div class="gla_shop_item">
                    <a href="{{asset('img/galeri/cincin.jpg')}}" class="lightbox">
                        <img src="{{asset('img/galeri/cincin.jpg')}}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-sm-4 gla_anim_box grid-item">
                <div class="gla_shop_item">
                    <a href="{{asset('img/galeri/1.jpeg')}}" class="lightbox">
                        <img src="{{asset('img/galeri/1.jpeg')}}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-sm-4 gla_anim_box grid-item">
                <div class="gla_shop_item">
                    <a href="{{asset('img/galeri/2.jpeg')}}" class="lightbox">
                        <img src="{{asset('img/galeri/2.jpeg')}}" alt="">
                    </a>
                </div>
            </div>
         </div>
    </div>
</section>
<script src="{{asset('glanz/js/glanz_library.js')}}"></script> 
<script src="{{asset('glanz/js/glanz_script.js')}}"></script>