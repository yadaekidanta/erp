<x-mobile-invitation-layout title="Undangan Tunangan : Eddy Mardiwan & Rianasari">
    <audio id="audio" loop>
        <source src="{{asset('music/source.mp3')}}" type="audio/mp3" />
    </audio>
    <div class="page-content pb-0">
        <div id="cover_content">
            <div class="card preload-img" data-src="" data-card-height="cover" style="background-image: url('{{asset('img/bg/1.png')}}');background-size:100% 100%;background-repeat:no-repeat;">
            {{-- <div class="card" data-card-height="cover"> --}}
                <div class="card-top mt-5 pt-5 text-center">
                    <h1 class="fa-2x font-900 color-white font_nanum">Engagement Ceremony</h1>
                    {{-- <h4 class="font-300 font-17 mt-2 color-white opacity-50">Beautfiful Mobile Intefaces</h4> --}}
                </div>
                <div class="card-center text-center" style="margin-top:-15%;">
                    {{-- <a class="col" data-gallery="gallery-1" href="{{asset('img/galeri/cincin.jpg')}}"> --}}
                    <img src="{{asset('img/galeri/cincin.jpg')}}" data-src="{{asset('img/galeri/cincin.jpg')}}" class="preload-img img-fluid rounded-circle" style="margin-top:35%;width:100px;height:100px;border:3px solid #D99B3C;;" alt="img">
                    {{-- </a> --}}
                    <h1 class="fa-4x font-300 pt-4" style="margin-top:5%;color:#D99B3C;">
                        Rianasari
                    </h1>
                    <h1 class="fa-4x font-300 pt-4" style="">
                        <span style="color:lightgrey;">&</span>
                    </h1>
                    <h1 class="fa-4x font-300 pt-4" style="color:#D99B3C;">
                        Eddy Mardiwan
                    </h1>
                    <p class="pt-3 color-white" style="margin-top:20%;">
                        Tanpa Mengurangi Rasa Hormat, kami mengundang Bapak/Ibu untuk menghadiri Prosesi Lamaran kami.
                    </p>
                    <h4 class="color-white font-300 font_nanum" style="margin-top:-5%;">
                        {{Carbon\Carbon::parse($tanggal)->isoFormat('dddd')}}, 
                        {{Carbon\Carbon::parse($tanggal)->isoFormat('D MMMM Y')}}
                        | 19:00 WIB
                    </h4>
                </div>
                <div class="card-bottom">
                    <a href="#" data-menu="menu-tips-1" class="btn btn-center-m color-black btn-m bg-white rounded-sm font-900 text-uppercase scale-box" style="margin-top:-35%;">Buka Undangan</a>
                </div>
                {{-- <div class="card-overlay bg-highlight"></div> --}}
                <div class="card-overlay bg-highlight opacity-0"></div>
            </div>
        </div>
        <div id="main_content">
            {{-- <div data-card-height="cover-card" class="card card-style"> --}}
            <div class="card preload-img" data-src="" data-card-height="cover" style="background-image: url('{{asset('img/bg/1.png')}}');background-size:100% 100%;background-repeat:no-repeat;">
            {{-- <div class="card" data-card-height="cover"> --}}
                <div class="card-top mt-5 pt-5 text-center">
                    <h1 class="fa-2x font-900 color-white font_nanum">Engagement Ceremony</h1>
                    {{-- <h4 class="font-300 font-17 mt-2 color-white opacity-50">Beautfiful Mobile Intefaces</h4> --}}
                </div>
                <div class="card-center text-center" style="margin-top:-15%;">
                    {{-- <a class="col" data-gallery="gallery-1" href="{{asset('img/galeri/cincin.jpg')}}"> --}}
                    <img src="{{asset('img/galeri/cincin.jpg')}}" data-src="{{asset('img/galeri/cincin.jpg')}}" class="preload-img img-fluid rounded-circle" style="margin-top:35%;width:100px;height:100px;border:3px solid #D99B3C;;" alt="img">
                    {{-- </a> --}}
                    <h1 class="fa-4x font-300 pt-4" style="margin-top:0%;color:#D99B3C;">
                        Rianasari
                    </h1>
                    <h1 class="fa-4x font-300 pt-4" style="">
                        <span style="color:lightgrey;">&</span>
                    </h1>
                    <h1 class="fa-4x font-300 pt-4" style="color:#D99B3C;">
                        Eddy Mardiwan
                    </h1>
                    <img src="{{asset('img/galeri/cincin.jpg')}}" data-src="{{asset('img/galeri/cincin.jpg')}}" class="preload-img img-fluid rounded-circle" style="margin-top:5%;width:100px;height:100px;border:3px solid #D99B3C;;" alt="img">
                    <h4 class="color-white font-300 font_nanum" style="margin-top:15%;">
                        {{Carbon\Carbon::parse($tanggal)->isoFormat('dddd')}}, 
                        {{Carbon\Carbon::parse($tanggal)->isoFormat('D MMMM Y')}}
                        | 19:00 WIB
                    </h4>
                </div>
                <div class="card-bottom text-center mb-5">
                    <div class="countdown row mb-4 mt-5 ms-2 me-2">
                        <div class="disabled">
                            <h1 class="mb-0 color-white font-30" id="years"></h1>
                            <p class="mt-n1 color-white font-11">Tahun</p>
                        </div>
                        <div class="col-3">
                            <h1 class="mb-0 color-white font-30" id="days"></h1>
                            <p class="mt-n1 color-white font-11">Hari</p>
                        </div>
                        <div class="col-3">
                            <h1 class="mb-0 color-white font-30" id="hours"></h1>
                            <p class="mt-n1 color-white font-11">Jam</p>
                        </div>
                        <div class="col-3">
                            <h1 class="mb-0 color-white font-30" id="minutes"></h1>
                            <p class="mt-n1 color-white font-11">Menit</p>
                        </div>
                        <div class="col-3">
                            <h1 class="mb-0 color-white font-30" id="seconds"></h1>
                            <p class="mt-n1 color-white font-11">Detik</p>
                        </div>	
                    </div>
                </div>
                {{-- <div class="card-overlay bg-highlight"></div> --}}
                <div class="card-overlay bg-highlight opacity-0"></div>
            </div>
            <div class="card preload-img" data-src="" data-card-height="cover" style="background-image: url('{{asset('img/bg/1.png')}}');background-size:100% 100%;background-repeat:no-repeat;">
                <div class="content mb-3" style="margin-top:30%;">
                    <h3 class="mb-0 color-white font_nanum">Cerita Kami</h3>
                    <p class="color-white" style="line-height:16px;">
                        Pertama kali liat Riri itu di Lampung tahun 1991. Ceritanya, kami bersama teman-teman sedang liburan di Pulau Sibesi, saat itu kami cuma sebatas sahabatan.
                        Namun, di tahun 2021 hadir cinta diantara kami saat bertemu kembali, dengan dukungan teman-teman kami, sehingga kami dapat bersatu sehingga bertahan sampai detik ini
                    </p>
                    <div class="row">
                        <div class="col-6">
                            <div class="text-center">
                                <img src="{{asset('img/mempelai/r.jpeg')}}" data-src="{{asset('img/mempelai/r.jpeg')}}" class="preload-img img-fluid rounded-circle" style="width:150px;height:140px;" alt="img">
                                <h1 class="fa-2x font-300 pt-2" style="color:#D99B3C;">
                                    Rianasari
                                </h1>
                                <h4 class="color-white font_nanum" style="margin-top:-5%;">
                                    Putri pertama dari<br>
                                <b>Ayah Daniel syarief</b><br>
                                <b>Ibu Herlina</b>
                                </h4>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-center">
                                <img src="{{asset('img/mempelai/e.jpg')}}" data-src="{{asset('img/mempelai/e.jpg')}}" class="preload-img img-fluid rounded-circle" style="width:150px;height:140px;" alt="img">
                                <h1 class="fa-2x font-300 pt-2" style="color:#D99B3C;">
                                    Eddy Mardiwan
                                </h1>
                                <h6 class="color-white font_nanum" style="margin-top:-5%; font-size:15px;">
                                    Putra ketiga dari <br>
                                    <b>Almarhum Soemarto Moedjaid</b><br>
                                    <b>Ibu Sani Soemarto Moedjaid</b>
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="card-overlay bg-fade-gray-light"></div> --}}
                <div class="card-overlay bg-theme opacity-0"></div>
            </div>
            <div class="card preload-img" data-src="" data-card-height="cover" style="background-image: url('{{asset('img/bg/1.png')}}');background-size:100% 100%;background-repeat:no-repeat;">
                <h3 class="mb-0 pt-3 text-center color-white" style="margin-bottom:10%;margin-top:20%;">Detail Acara</h3>
                <div class="card card-style preload-img" data-src="{{asset('img/galeri/cincin.jpg')}}" data-card-height="175" style="margin-top:10%;height: 175px;">
                    {{-- <div class="card-top">
                        <a href="https://goo.gl/maps/H3NNSAH8bpF2TsdU9" target="_blank">
                            <i class="fa fa-map-marker-alt color-brown-dark fa-3x float-start ms-3 mt-3"></i>
                        </a>
                    </div> --}}
                    <div class="content mb-0">
                        <div class="float-start">
                            <h1 class="mb-n1 color-white font_nanum">Pertunangan</h1>
                            <p class="font-10 mb-2 pb-1 color-white" style="line-height:14px;margin-top:42%;"><i class="fa fa-map-marker-alt me-2"></i>
                                Jl. Teuku Cik Ditiro No.35, RT.10/RW.5<br>
                                Menteng, Kec. Menteng, Kota Jakarta Pusat<br>
                                Daerah Khusus Ibukota Jakarta 10310
                            </p>
                        </div>
                        <a href="https://goo.gl/maps/H3NNSAH8bpF2TsdU9" target="_blank" class="float-end btn btn-s bg-highlight rounded-xl shadow-xl text-uppercase font-900 font-11 mt-2">Lihat Peta</a>
                    </div>
                    <div class="card-bottom ms-3">
                        <p class="color-white font-10 opacity-0 mb-2"><i class="far fa-calendar"></i> {{Carbon\Carbon::parse($tanggal)->isoFormat('D MMMM Y')}} <i class="ms-3 far fa-clock"></i> 19.00 WIB</p>
                        {{-- <p class="color-white font-10 opacity-80 mb-2"><i class="fa fa-map-marker-alt"></i> Melbourne, Victoria, Australia Collins Street</p> --}}
                    </div>
                </div>
            </div>
            <div class="card preload-img" data-src="" data-card-height="cover" style="background-image: url('{{asset('img/bg/1.png')}}');background-size:100% 100%;background-repeat:no-repeat;">
                <div class="card card-style">
                    <div class="splide single-slider slider-no-arrows slider-no-dots" id="single-slider-2">
                        <div class="splide__track">
                            <div class="splide__list">
                                <div class="splide__slide">
                                    <div data-card-height="300" class="card mb-0 bg-18 shadow-l" data-src="{{asset('img/bg/cover.png')}}">
                                        <div class="card-bottom text-center mb-3">
                                            <h2 class="color-white text-uppercase font-900 mb-0">Splendid Simplicity</h2>
                                            <p class="under-heading color-white">A new generation of Mobile Kits.</p>
                                        </div>
                                        <div class="card-overlay bg-gradient"></div>
                                    </div>
                                </div>
                                <div class="splide__slide">
                                    <div data-card-height="300" class="card mb-0 bg-14 shadow-l">
                                        <div class="card-bottom text-center mb-3">
                                            <h2 class="color-white text-uppercase font-900 mb-0">Top Notch Quality</h2>
                                            <p class="under-heading color-white">Flexibility, Speed, Ease of Use.</p>
                                        </div>
                                        <div class="card-overlay bg-gradient"></div>
                                    </div>
                                </div>
                                <div class="splide__slide">
                                    <div data-card-height="300" class="card mb-0 bg-14 shadow-l">
                                        <div class="card-bottom text-center mb-3">
                                            <h2 class="color-white text-uppercase font-900 mb-0">Perfectly Organized</h2>
                                            <p class="under-heading color-white">Mobile Website, or App or PWA Ready.</p>
                                        </div>
                                        <div class="card-overlay bg-gradient"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div class="card preload-img" data-src="" data-card-height="cover" style="background-image: url('{{asset('img/bg/1.png')}}');background-size:100% 100%;background-repeat:no-repeat;">
                <div class="card card-style contact-form preload-img" style="margin-top:30%;">
                    <div class="card-body py-4">
                        <div class="content">
                            <h3 class=">Doa & Ucapan</h3>
                            <form action="php/contact.php" method="post" class="contactForm" id="contactForm">
                                <fieldset>
                                    <div class="form-field form-name">
                                        <label class="contactNameField color-theme font_nanum" for="contactNameField">Nama:</label>
                                        <input type="text" name="contactNameField" value="" class="round-small" id="contactNameField" />
                                    </div>
                                    <div class="form-field form-text">
                                        <label class="contactMessageTextarea color-theme font_nanum" for="contactMessageTextarea">Pesan untuk Mempelai:</label>
                                        <textarea name="contactMessageTextarea" class="round-small" id="contactMessageTextarea"></textarea>
                                    </div>
                                    <div class="form-field form-email d-none">
                                        <label class="contactEmailField color-theme" for="contactEmailField">Kehadiran:</label>
                                        
                                    </div>
                                    <div class="form-button">
                                        <input type="submit" class="btn bg-highlight text-uppercase font-900 btn-m btn-full rounded-sm  shadow-xl contactSubmitButton" value="Kirim" data-formId="contactForm" />
                                    </div>
                                </fieldset>
                            </form>
                            <br>
                            <p class="font_nanum">
                                Atas doa & ucapan bapak/ibu/saudara/i, Kami mengucapkan terima kasih.<br><br>
                                Salam,<br>
                                Rianasari & Eddy Mardiwan
                            </p>	
                        </div>
                    </div>
                    <div class="card-overlay bg-white opacity-90"></div>
                    <div class="card-overlay dark-mode-tint"></div>
                </div>
            </div>
            <div class="card preload-img" data-src="" data-card-height="cover" style="background-image: url('{{asset('img/bg/1.png')}}');background-size:100% 100%;background-repeat:no-repeat;">
                <div class="card-body py-4" style="margin-top:20%;">
                    <h2 class="color-white font-600 mb-4 text-center font_nanum">Doa & Ucapan dari undangan</h2>
                    {{-- <p class="color-white mb-4 opacity-70">
                        Enjoy your spotlight. We'll capture every moment.
                    </p> --}}
                    <div class="card card-style py-3 ms-0 me-0 mb-0 bg-theme">
                        <div class="d-flex px-3 mb-4">
                            <div class="pe-3 align-self-center">
                                <h5 class="font-700 mb-0 font-17 font_nanum">Joko Pitoyo</h5>
                                <p class="mb-0 font_nanum">
                                    Sejatinya pernikahan adalah lembaran baru kehidupan, kebahagiaan, kebersamaan, dan hal-hal baik lainnya yang menyertai.
                                </p>
                            </div>
                            {{-- <div>
                                <img src="images/pictures/6s.jpg" width="110" class="rounded-sm">
                            </div> --}}
                        </div>
                        <div class="d-flex px-3 mb-4">
                            <div class="pe-3 align-self-center">
                                <h5 class="font-700 mb-0 font-17 font_nanum">Kosim</h5>
                                <p class="mb-0 font_nanum">
                                    Mantap!! Selamat berbahagia menjalani bahtera rumah tangga yang baru.
                                </p>
                            </div>
                        </div>
                        <div class="d-flex px-3 mb-4">
                            <div class="pe-3 align-self-center">
                                <h5 class="font-700 mb-0 font-17 font_nanum">Abdul</h5>
                                <p class="mb-0 font_nanum">
                                    Yeay!! Selamat ya, akhirnya kalian nikah juga :p
                                </p>
                            </div>
                        </div>
                        {{-- <a href="#" class="btn btn-margins mt-2 btn-m bg-highlight font-700 text-uppercase mb-0 rounded-sm shadow-xxl">Book a Photoshoot</a> --}}
                    </div>
                </div>
                <div class="card-overlay dark-mode-tint"></div>
            </div>
        </div>
    </div>
    <div id="menu-tips-1" 
         class="menu menu-box-modal menu-box-detached rounded-m" 
         data-menu-height="520" 
         data-menu-width="340" 
         data-menu-effect="menu-over">
        <div class="card header-card" data-card-height="250" style="margin-top:-25%;margin-right:-20%;">
            <div class="card-overlay bg-highlight opacity-0"></div>
            <div class="card-overlay dark-mode-tint"></div>
            <div class="card-bg preload-img" data-src="{{asset('img/bg/modal.jpeg')}}"></div>
        </div>
        <div class="mt-3 pt-1 pb-1">
            <h1 class="text-center">
                <i data-feather="smartphone" 
                   data-feather-line="1" 
                   data-feather-size="60" 
                   data-feather-color="gray-dark" 
                   data-feather-bg="none"></i>
            </h1>
            <h1 class="text-center color-white font-14 font-700">PWA Ready</h1>
            <p class="text-center mt-n3 mb-3 font-11 color-white">Just add it to your home screen and Enjoy!</p>
        </div>
        <div class="card card-style">
            <h2 class="text-center font-700 mt-2 font_nanum">Informasi Protokol Kesehatan</h2>
            <p class="boxed-text-xl font_nanum">Acara ini akan dilaksanakan dengan menerapkan protokol kesehatan.</p>
            <div class="row text-center row-cols-3 mb-0">
                <a class="col" data-gallery="gallery-1" href="{{asset('img/prokes/1.png')}}" title="Vynil and Typerwritter">
                    <img src="{{asset('img/prokes/1.png')}}" data-src="{{asset('img/prokes/1.png')}}" class="preload-img img-fluid rounded-circle" alt="img">
                    <p class="font-600 pb-1 font_nanum">Memakai Masker</p>
                </a>
                <a class="col" data-gallery="gallery-1" href="{{asset('img/prokes/2.png')}}" title="Cream Cookie">
                    <img src="{{asset('img/prokes/2.png')}}" data-src="{{asset('img/prokes/2.png')}}" class="preload-img img-fluid rounded-circle" alt="img">
                    <p class="font-600 pb-1 font_nanum">Menjaga Jarak</p>
                </a>
                <a class="col" data-gallery="gallery-1" href="{{asset('img/prokes/3.png')}}" title="Cookies and Flowers">
                    <img src="{{asset('img/prokes/3.png')}}" data-src="{{asset('img/prokes/3.png')}}" class="preload-img img-fluid rounded-circle" alt="img">
                    <p class="font-600 pb-1 font_nanum">Mencuci Tangan</p>
                </a>
            </div>
        </div>
        <div class="row mb-0" style="margin-top:-7%;">
            <div class="col-2"></div>
            <div class="col-8">
                <a href="#" id="buka_undangan" onclick="buka_undangan();" class="close-menu btn btn-sm me-3 rounded-s btn-full shadow-l bg-highlight font-900 text-uppercase">Baik, Saya Mengerti</a>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
    @section('custom_js')
        <script type="text/javascript">
        var audio = document.getElementById("audio");
        $("#main_content").hide();
        function buka_undangan(){
            countdown('03/20/2022 07:00:00 PM');
            $("#cover_content").hide();
            $("#main_content").show();
            // audio.play();
        }
        </script>
    @endsection
</x-mobile-invitation-layout>