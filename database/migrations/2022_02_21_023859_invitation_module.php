<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InvitationModule extends Migration
{
    public function up()
    {
        Schema::create('covers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('photo');
        });
    }
    public function down()
    {
        Schema::dropIfExists('covers');
    }
}
